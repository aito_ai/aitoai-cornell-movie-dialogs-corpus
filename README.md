# Aito.ai implementation of the Cornell Movie-Dialogs Corpus

This dataset has been adapted to Aito.ai 
from the [Cornell Movie-Dialogs Corpus](http://www.cs.cornell.edu/~cristian/Cornell_Movie-Dialogs_Corpus.html). 

The original zip-file is available here: http://www.cs.cornell.edu/~cristian/data/cornell_movie_dialogs_corpus.zip

No modifications to the data have been done, other than to format it properly for use with the database. 

For comments or questions regarding the original data set, please turn to the source. For comments or questions
regarding aito.ai and/or the use of the dataset in it, please contact us at tech@aito.ai

## Use
Please checkout the `setup_sequence.sh` script, which contains the initial logic for setting up the database
with the provided data. You'll need:

1. Bash
1. An aito-instance, and read-write access to it
1. Httpie (https://httpie.org/)
1. Xz (https://en.wikipedia.org/wiki/Xz), which is available through Homebrew on a Mac, and any package manger in linux

