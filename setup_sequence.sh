#!/bin/bash -e

# To use this script you need
# 1. An aito.ai account, so you can provision the data into the database
#
# 2. A read-write access key to the database
#
# 3. HTTPie (https://httpie.org/), or alternatively, you can convert httpie to curl, by changing the script and
# parameter format. Please checkout httpie, though. It's cool!

command -v xz | grep -c . > /dev/null || ( echo "You seem to be missing xz compression tool" && exit 1 )
command -v http | grep -c . > /dev/null  || ( echo "You don't have httpie installed" && exit 2 )

# The Aito.ai API-key
API_KEY='x-api-key: aitoai-api-key-ask-for-your-own'
SERVER_HOST=localhost

echo "Provisioning the schema"
time cat corpusSchema.json| http --timeout 30000 PUT ${SERVER_HOST}/api/schema/_mapping "$API_KEY"

echo "Provisioning the movies"
time xzcat movies.json.xz | http --timeout 3000 POST ${SERVER_HOST}/api/data/movie/batch "$API_KEY"

echo "Provisioning characters"
time xzcat movie_characters.json.xz | http --timeout 3000 POST ${SERVER_HOST}/api/data/character/batch "$API_KEY"

for file in $(find chunked -type f -name movie_lines.json.\*); do
    echo "Importing file $file"
    time xzcat $file | http --timeout 3000 POST ${SERVER_HOST}/api/data/line/batch "$API_KEY"
done
