#!/bin/bash

counter=0

# https://unix.stackexchange.com/questions/370306/split-a-sting-field-into-an-array-in-jq
for line in $(cat single_lines2.text \
		  | jq -c 'map(.conversation |= split(","))' \
		  | perl -pe 's!" !"!g' \
		  | perl -pe 's!^\[!!g' \
		  | perl -pe 's!\]$!!g' \
		  | perl -pe 's!},{!}\n{!g' \
		  | grep conversation); do

    conversation_id=$(uuidgen | tr [A-Z] [a-z])
    previous=null

    for utt in $(echo $line | jq '.conversation | .[]' | perl -pe 's!"!!g' ); do
	new_object=$(jq -c '.[] | select(.lineId=="'$utt'")' movie_lines.json)

	echo $new_object | jq -c --arg cid $conversation_id --arg prev $previous '. + {previousLine: $prev, conversationId: $cid}' >> denormalised_movie_lines.json
	previous=$utt
    done

    counter=$(( $counter + 1 ))

    echo "Finished parsing $counter lines"
done
