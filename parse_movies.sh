#!/bin/bash

jq -Rsn '
    [inputs
     | . / "\n"
     | ( .[] | select((. | length) > 0) | . / " +++$+++ ") as $input
     | {"movieId": $input[0], "title": $input[1], "year": $input[2], "imdbRating": $input[3], "imdbVoteCount": $input[4], "genres": $input[5]}]
' <movie_titles_metadata.txt | \
perl -pe 's!("year": )"(\d+)"!$1$2!g' | perl -pe 's!("imdbRating": )"([\d\.]+)"!$1$2!g' | perl -pe 's!("imdbVoteCount": )"([\d\.]+)"!$1$2!g' > movies.json


jq -Rsn '
    [inputs
     | . / "\n"
     | ( .[] | select((. | length) > 0) | . / " +++$+++ ") as $input
     | {"characterId": $input[0], "characterName": $input[1], "movieId": $input[2], "movieTitle": $input[3], "gender": $input[4], "positionInCredits": $input[5]}]
' <movie_characters_metadata.txt

jq -Rsn '
    [inputs
     | . / "\n"
     | ( .[] | select((. | length) > 0) | . / " +++$+++ ") as $input
     | {"lineId": $input[0], "characterId": $input[1], "movieId": $input[2], "characterName": $input[3], "utterance": $input[4]}]
' <movie_lines.txt


jq -Rsn '
    [inputs
     | . / "\n"
     | ( .[] | select((. | length) > 0) | . / " +++$+++ ") as $input
     | {"firstCharacter": $input[0], "secondCharacter": $input[1], "movieId": $input[2], "conversation": $input[3]}]
' <movie_conversations.txt



